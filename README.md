# Face Morpher

A ROS package with a node to fuse multiple face point clouds into a single model.

A mesh of a human face which is topologically coherent can be morphed into the captured face.

