// ROS
#include <ros/ros.h>
#include <ros/package.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <pcl_ros/point_cloud.h>
#include <pcl_conversions/pcl_conversions.h>

// OpenCV
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

// Point Cloud Library
#include <pcl/io/ply_io.h>
#include <pcl/search/impl/search.hpp>
#include <pcl/filters/filter.h>
#include <pcl/filters/crop_box.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/features/normal_3d.h>
#include <pcl/features/integral_image_normal.h>
#include <pcl/registration/icp.h>
#include <pcl/features/principal_curvatures.h>
#include <pcl/octree/octree.h>

// CPU_TSDF
#include <cpu_tsdf/marching_cubes_tsdf_octree.h>

#include <face_morpher/face_morpher.h>

FaceMorpher::FaceMorpher(ros::NodeHandle& nh)
    : camera_nh(nh)
    , cloud(new PointCloud)
    , last_cloud(new PointCloud)
    , aligned_cloud(new PointCloud)
    , aligned_cloud2(new PointCloud)
    , segmented_cloud(new PointCloud)
    , normals(new PointCloudWithNormals)
    , segmented_normals(new PointCloudWithNormals)
    , tsdf(new cpu_tsdf::TSDFVolumeOctree)
    , pose_global(Eigen::Matrix4f::Identity())
    , morphing(false)
{
    // Build the TSDF volume
    tsdf->setGridSize(2.0, 2.0, 2.0);       // grid size in meters
    tsdf->setResolution(1024, 1024, 1024);  // cell resolution
    tsdf->setIntegrateColor(true);
    tsdf->reset();
}

void FaceMorpher::visualize()
{
    if (!viewer.get()) {
        viewer.reset(new pcl::visualization::PCLVisualizer("Viewer"));
        viewer->setFullScreen(false);
        viewer->registerKeyboardCallback(FaceMorpher::keyboardEventOccurred, (void*) this);
        viewer->setBackgroundColor(0.3, 0.3, 0.3);
        viewer->setCameraPosition(0.5, -0.1, -0.7, 0, 0, 1, 0, -1, 0);
        viewer->addPointCloud(segmented_cloud, "segmented_cloud");
        viewer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 3, "segmented_cloud");
    }

    while (!viewer->wasStopped() && ros::ok()) {
        viewer->spinOnce(100);
        // Get lock on the boolean update and check if cloud was updated
        boost::mutex::scoped_lock lock(updateModelMutex);
        if (update && cloud->width) {
            viewer->updatePointCloud(segmented_cloud, "segmented_cloud");
            viewer->removePointCloud("normals", 0);
            viewer->addPointCloudNormals<PointT, NormalT>(segmented_cloud, segmented_normals, 20, 0.01, "normals");
            viewer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_COLOR, 1.0, 0.0, 0.0, "normals");
            viewer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_LINE_WIDTH, 1, "normals");
            update = false;
        }
        lock.unlock();
    }
}

void FaceMorpher::rgbdCb(const sensor_msgs::PointCloud2ConstPtr& pc)
{
    if (morphing) return;
    start_time = (double)cv::getTickCount();
    frame_counter = 0;

    // Create mutex to synchronize with the visualizer
    boost::mutex::scoped_lock updateLock(updateModelMutex);
    update = true;

    // Convert the point cloud into the PCL format
    pcl::fromROSMsg(*pc, *cloud);
    frame_counter = (frame_counter % std::numeric_limits<int>::max()) + 1;

    // Remove NaN points for ICP registration
    std::vector<int> indices_nan;
    pcl::removeNaNFromPointCloud(*cloud, *segmented_cloud, indices_nan);

    // Crop just the face with a box filter
    pcl::CropBox<PointT> crop_filter(true); // Record removed indices
    crop_filter.setMin(Eigen::Vector4f(-.5, -.60, 0.5, 1));
    crop_filter.setMax(Eigen::Vector4f(0.5, -.02, 1.5, 1));
    crop_filter.setInputCloud(segmented_cloud);
    crop_filter.filter(*segmented_cloud);
    int per = 100 * ((int) segmented_cloud->size()) / ((int) cloud->size());
    ROS_DEBUG("Filter results: %d -> %d (%d%%)", (int) cloud->size(), (int) segmented_cloud->size(), per);

    // Estimate normals over the organized point cloud
    pcl::IntegralImageNormalEstimation<PointT, NormalT> ne;
    ne.setNormalEstimationMethod(ne.AVERAGE_3D_GRADIENT);
    ne.setMaxDepthChangeFactor(0.02f);
    ne.setNormalSmoothingSize(10.0f);
    ne.setInputCloud(cloud);
    ne.compute(*normals);

    // Filter the normal map: remove NaN points and the outside cropped volume
    pcl::ExtractIndices<NormalT> remove_nan;
    pcl::IndicesPtr indices(new std::vector<int>);
    indices->insert(indices->end(), indices_nan.begin(), indices_nan.end());
    remove_nan.setInputCloud(normals);
    remove_nan.setIndices(indices);
    remove_nan.filter(*segmented_normals);

    pcl::ExtractIndices<NormalT> extract_normals;
    extract_normals.setInputCloud(segmented_normals);
    extract_normals.setIndices(crop_filter.getRemovedIndices());
    extract_normals.setNegative(true);
    extract_normals.filter(*segmented_normals);

    // Register the last two point clouds using the ICP algorithm
    if (last_cloud->size()) {
        // Compute rigid body transformation to align the input cloud to the target cloud
        pcl::IterativeClosestPoint<PointT, PointT> icp;
        icp.setMaxCorrespondenceDistance(0.025);
        icp.setMaximumIterations(50);
        icp.setTransformationEpsilon(1e-8);
        icp.setEuclideanFitnessEpsilon(1);
        icp.setInputSource(last_cloud);
        icp.setInputTarget(segmented_cloud);
        PointCloud icp_result;
        icp.align(icp_result);

        // Print info about about the registration
        if (icp.hasConverged()) {
            // Compute pose transformation relative to first camera pose
            pose_global = icp.getFinalTransformation() * pose_global;

            // Crop the face in the organized cloud to integrate
            crop_filter.setKeepOrganized(true);
            crop_filter.setInputCloud(cloud);
            crop_filter.filter(*cloud);

            // Integrate the cloud into a TSDF volume (by @sdmiller)
            tsdf->integrateCloud(*cloud, *cloud, Eigen::Affine3d(pose_global.cast<double>()));
//            pcl::transformPointCloud(*segmented_cloud, *aligned_cloud, pose_global);
            this->morph();

            ROS_DEBUG("ICP registration success");
            std::cout << "------------------" << std::endl << pose_global << std::endl;
        } else {
            ROS_DEBUG("ICP registration did not converge");
        }
    }

    updateLock.unlock();

    // Update the last cloud with the current one
    pcl::copyPointCloud(*segmented_cloud, *last_cloud);

    // Compute the fps
    double ts = ((double) cv::getTickCount() - start_time) / cv::getTickFrequency();
    ROS_DEBUG("Running at %f fps", frame_counter / ts);
}

void FaceMorpher::keyboardEventOccurred(const pcl::visualization::KeyboardEvent &event, void* morpher_pointer)
{
    FaceMorpher *morpher = (FaceMorpher *) morpher_pointer;
    if (event.getKeySym() == "s" && event.keyDown())
        morpher->morph();
}

void FaceMorpher::morph()
{
    ROS_DEBUG("Morphing the mesh with the canonical human face ...");

    // Load the mesh for the human face from the package location
    pcl::PolygonMesh mesh_low;
    pcl::io::loadPLYFile(ros::package::getPath(PACKAGE_NAME) + "/data/human-face.ply", mesh_low);

    // Reconstruct the mesh with Marching cubes applied over the TSDF volume
    morphing = true;
    cpu_tsdf::MarchingCubesTSDFOctree mc;
    mc.setInputTSDF(tsdf);
    mc.setMinWeight(1); // The minimum weight of a point in a voxel to create a triangle
    mc.setColorByRGB(true);
    pcl::PolygonMesh mesh_high;
    mc.reconstruct(mesh_high);
    pcl::io::savePLYFile("mc_mesh.ply", mesh_high);
    ROS_DEBUG("The mesh has been written to mc_mesh.ply");

    // Load point clouds from both meshes
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_high (new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_low (new pcl::PointCloud<pcl::PointXYZ>);
    pcl::fromPCLPointCloud2 (mesh_high.cloud, *cloud_high);
    pcl::fromPCLPointCloud2 (mesh_low.cloud, *cloud_low);

    // Build a KD-tree for the kinect mesh to search points efficiently
    pcl::search::KdTree<pcl::PointXYZ> tree(true);
    tree.setInputCloud(cloud_high);

    for (size_t i = 0; i < cloud_low->size(); ++i) {
        pcl::PointXYZ p = cloud_low->points[i];
        std::vector<int> pointIdx;
        std::vector<float> distances;
        tree.nearestKSearch(p, 3, pointIdx, distances);

        // Compute the baricenter of the three nearest points
        Eigen::Vector4f r(0, .0, .0, .0);
        for (size_t j = 0; j < pointIdx.size(); ++j) {
            r = r + cloud_high->at(pointIdx[j]).getVector4fMap();
        }
        r = r / 3.0;
    }

    // Shutdown the ROS node
    //ros::shutdown();
}


void FaceMorpher::rgbCb(const sensor_msgs::ImageConstPtr& img_msg)
{
    // Recover OpenCV Mat from ROS message
    cv::Mat frame;
    try {
        frame = cv_bridge::toCvShare(img_msg, sensor_msgs::image_encodings::TYPE_32FC1)->image;
    }
    catch (cv_bridge::Exception& e) {
        ROS_ERROR("cv_bridge exception: %s", e.what());
        return;
    }

    // Remove some noise with a Gaussian filter
    cv::GaussianBlur(frame, frame, cv::Size(5, 5), 1.0);

    // Scale the 4 bytes depth pixels to a single byte (unsigned char)
    float min_range = 0.5;
    float max_range = 5.5;
    cv::Mat img(frame.rows, frame.cols, CV_8UC1);
    for (int i = 0; i < frame.rows; i++) {
        float* Di = frame.ptr<float>(i);
        char* Ii = img.ptr<char>(i);
        for (int j = 0; j < frame.cols; ++j)
            Ii[j] = (char)(255 * ((Di[j] - min_range) / (max_range - min_range)));
    }

    cv::imshow("depth", frame);
    cv::waitKey(30);
}
