#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <face_morpher/face_morpher.h>

int main(int argc, char** argv)
{
    ros::init(argc, argv, PACKAGE_NAME);
    ros::NodeHandle nh("camera");

    // Parse arguments
    std::string topic;
    ros::param::param<std::string>("input", topic, "depth_registered/points");
    if (!topic.compare("depth_registered/points"))
        ROS_WARN("the point cloud topic is not specified: depth_registered/points is used by default");

    FaceMorpher morpher(nh);

    // Subscribe to the camera's point cloud
    ros::Subscriber sub = nh.subscribe<sensor_msgs::PointCloud2>(topic, 1, &FaceMorpher::rgbdCb, &morpher);

    // Subscribe to the depth map image
//    image_transport::ImageTransport it(nh);
//    image_transport::Subscriber isub = it.subscribe("depth_registered/image_raw", 1, &FaceMorpher::rgbCb, &morpher);

    std::cout << "Press 's' to save the current TSDF volume to a mc_mesg.ply file on disks" << std::endl;

    // Launch callback listeners and viewer in separate threads
    ros::AsyncSpinner spinner(1);
    spinner.start();
    morpher.visualize();
    ros::waitForShutdown();
}
