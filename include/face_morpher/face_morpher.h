#ifndef CAMERATRACKER_H
#define CAMERATRACKER_H

#define PACKAGE_NAME "face_morpher"

#include <ros/ros.h>
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/Image.h>
#include <pcl/visualization/cloud_viewer.h>
#include <pcl/visualization/pcl_visualizer.h>

#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl/PolygonMesh.h>

#include <cpu_tsdf/tsdf_volume_octree.h>

typedef pcl::PointXYZRGB PointT;
typedef pcl::PointCloud<PointT> PointCloud;
typedef pcl::PointNormal NormalT;
typedef pcl::PointCloud<NormalT> PointCloudWithNormals;

class FaceMorpher {
public:
    FaceMorpher(ros::NodeHandle& nh);

    /**
     * @brief Receives a point cloud message
     * @param pc The new image message from the camera topic
     */
    void rgbdCb(const sensor_msgs::PointCloud2ConstPtr& pc);

    /**
     * @brief Handle keyboard events
     * @param event The keyboard event
     * @param p Pointer to the event sender (actually, a way to pass a reference to this)
     */
    static void keyboardEventOccurred(const pcl::visualization::KeyboardEvent &event, void* p);

    /**
     * @brief Callback to process images
     * @param img The image captured from topic
     */
    void rgbCb(const sensor_msgs::ImageConstPtr &img_msg);

    /**
     * @brief Visualize the point cloud with normals
     */
    void visualize();

    /**
     * @brief Morph the Kinect's mesh into the mesh
     */
    void morph();

private:

    ros::NodeHandle camera_nh;
    pcl::visualization::PCLVisualizer::Ptr viewer;
    bool update, morphing;
    boost::mutex updateModelMutex;

    PointCloud::Ptr cloud, last_cloud, aligned_cloud, aligned_cloud2, segmented_cloud;
    PointCloudWithNormals::Ptr normals, segmented_normals;

    cpu_tsdf::TSDFVolumeOctree::Ptr tsdf;
    Eigen::Matrix4f pose_global;

    double start_time;
    unsigned int frame_counter;
    unsigned int min_matches;
    unsigned int n_features;
};

#endif // CAMERATRACKER_H
